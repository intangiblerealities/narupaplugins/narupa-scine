﻿using System;
using System.IO;
using System.Reflection;
using Nano;
using Nano.Loading;
using Nano.Server.Plugins;
using ScineForceFieldTests.Util;

namespace ScineForceFieldTests
{
    public class TestHelper
    {
        private static readonly Random Random = new Random();

        public IReporter Reporter = new CmdReporter();

        public int Seed { get; }

        private string AssemblyLocation()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Uri codebase = new Uri(assembly.CodeBase);
            string path = codebase.LocalPath;

            return new FileInfo(path).DirectoryName + @"\";
        }

        public TestHelper()
        {
            //string workDir = Helper.ResolvePath(NUnit.Framework.TestContext.CurrentContext.WorkDirectory + "/");
            Helper.ApplicationRootPath = AssemblyLocation();
            string pluginDir = Helper.ResolvePath(Helper.ApplicationRootPath + "../../../../../narupa-server/bin/Plugins");
            if(Directory.Exists(pluginDir) == false)
                throw new Exception($"Plugin directory {pluginDir} does not existed. Required for this test!");

            LoadManager.ScanAssembly(typeof(Nano.Server.AssemblyRoot).Assembly);
            LoadManager.ScanAssembly(typeof(Nano.AssemblyRoot).Assembly);
            LoadManager.ScanAssembly(typeof(Nano.Science.Simulation.AssemblyRoot).Assembly);
            LoadManager.ScanAssembly(typeof(Narupa.MD.AssemblyRoot).Assembly);

            PluginLoader.ScanPluginDirectory(Reporter, pluginDir);

            Seed = System.DateTime.Now.Millisecond;
            MathUtilities.Rand = new Random(Seed);

            Reporter.PrintEmphasized("The seed used for this test was: {0} ", Seed);
        }

    }
}