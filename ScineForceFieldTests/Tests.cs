﻿using NUnit.Framework;
using Narupa.MD.System;

namespace ScineForceFieldTests
{
    [TestFixture]
    public class Tests
    {
        private TestHelper helper;

        [SetUp]
        public void Initialize()
        {
            //Test helper sorts out loading the plugin for us.
            helper = new TestHelper();
            
        }
        
        [Test]
        public void TestPotentialEnergy()
        {
            string simFile = "^/Assets/Propyne.xml";
            Simulation sim = new Simulation(simFile, helper.Reporter);
            float energy = sim.ActiveSystem.CalculateForceFields();
            //Check that the energy is as expected.
            Assert.AreEqual(-40560.07f, energy, 0.01f);
            sim.Dispose();
        }
    }
}