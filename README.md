# README #

This is the repository for a C# wrapper to SCINE (developed by [Marcus Reiher's](http://www.reiher.ethz.ch/) group). 

The SCINE Sparrow plugin allows you to calculate electronic energies, nuclear
gradients as well as nuclear Hessians for arbitrary molecules with a wide range
of fast quantum chemical semiempirical methods such as PM6 and OMx.

Currently, only Windows is supported.

### Installing 

## Windows 

Download the appropriate version of the plugin from [here](https://gitlab.com/glowackigroup/narupa-scine/tags), and place the entire folder in the Plugins folder of your installation. Be sure to download the accompanying parameters as well (distributed separately due to upload size limitations), and place them in the installation folder. 

### Building from source

The repository contains a Visual Studio solution and associated projects. 

The repository should be cloned to the plugins folder, which is at the same level as the server, as shown below 
This repository references the Release builds of the engine, so they should be built. 

```
Narupa
+-- narupa-server
+-- plugins
|   +-- narupa-scine
```

## Compiling on Windows

Open the ScineForceField Visual Studio solution. 

Build the ScineForceField project. 

The project will automatically build and install the plugin to the Plugins folder of the engine. Rebuild the engine.

## Acknowledgements 

If you use this plugin, please cite the following original reference where the
software has been described:

T. Husch, A. C. Vaucher, M. Reiher, "Semiempirical Molecular Orbital Models
Based on the Neglect of Diatomic Differential Overlap Approximation", Int. J.
Quantum Chem. 118 (2018) e25799.

If you use one of the DFTB methods, please also cite the following reference:

M. P. Haag, A. C. Vaucher, M. Bosson, S. Redon, M. Reiher, "Interactive
Chemical Reactivity Exploration", ChemPhysChem 15 (2014) 3301.


For more information on the software see: scine.ethz.ch/download/sparrow
