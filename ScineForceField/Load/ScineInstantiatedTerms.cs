﻿using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;

namespace ScineForceField.Load
{
    class ScineInstantiatedTerms : IInstantiatedForceFieldTerms
    {
        public ScineForceField.QCMethod Method { get; internal set; }
        public string ParamsDirectory { get; internal set; }

        public IForceField GenerateForceField(InstantiatedTopology parentTopology, SystemProperties properties, IReporter reporter)
        {
            return new ScineForceField(this, parentTopology, properties.SimBoundary, reporter);

        }
    }
}
