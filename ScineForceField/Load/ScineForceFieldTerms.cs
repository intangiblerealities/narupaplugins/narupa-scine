﻿using Nano.Science.Simulation.ForceField;
using Nano.Loading;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Residues;
using Nano.Science.Simulation.Spawning;
using System.Xml;
using Nano;

namespace ScineForceField.Load
{
    [XmlName("ScineForceField")]
    class ScineForceFieldTerms : IForceFieldTerms
    {
        internal string ParamsDirectory = "^/Plugins/ScineForceField/Params";
        internal ScineForceField.QCMethod Method = ScineForceField.QCMethod.DFTB1;

        public void AddToTopology(InstantiatedTopology parentTopology, IResidue residue, ref SpawnContext context)
        {
            ScineInstantiatedTerms flattenedTerms = null;
            bool termExists = false;

            foreach (IInstantiatedForceFieldTerms forceFieldTermType in parentTopology.ForceFieldTerms)
            {
                if (forceFieldTermType is ScineInstantiatedTerms)
                {
                    flattenedTerms = forceFieldTermType as ScineInstantiatedTerms;
                    termExists = true;
                    break;
                }
            }

           
            //If it doesn't exist, create it.
            if (termExists == false)
            {
                flattenedTerms = new ScineInstantiatedTerms();
            }

            flattenedTerms.ParamsDirectory = this.ParamsDirectory;
            flattenedTerms.Method = this.Method;
            if (termExists == false)
            {
                parentTopology.ForceFieldTerms.Add(flattenedTerms);
            }
        }

        /// <summary>
        /// Loads instance.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="node"></param>
        public void Load(LoadContext context, XmlNode node)
        {
            ParamsDirectory = Helper.GetAttributeValue(node, "ParameterDirectory", ParamsDirectory);
            Method = Helper.GetAttributeValue(node, "Method", Method);
        }

        /// <summary>
        /// Saves instance.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="element"></param>
        /// <returns></returns>
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "ParameterDirectory", ParamsDirectory);
            Helper.AppendAttributeAndValue(element, "Method", Method);
            return element;
        }
    }
}
