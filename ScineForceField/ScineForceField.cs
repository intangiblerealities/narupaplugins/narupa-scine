﻿using Nano.Science.Simulation.ForceField;
using System;
using System.Collections.Generic;
using System.IO;
using Nano.Science.Simulation;
using SlimMath;
using Nano;
using ScineForceField.Load;
using System.Runtime.InteropServices;
using Nano.Science.Simulation.Instantiated;
using Nano.Science;

namespace ScineForceField
{
    class ScineForceField : IForceField
    {
        public enum QCMethod { PM6, DFTB1, DFTB2, DFTB3}


        [DllImport("iSciPM6", EntryPoint = "Init")]
        private static extern void Init([In] string paramsDir, int natoms, [In] int[] elements);

        [DllImport("iSciPM6", EntryPoint = "CalculateForceField")]
        private static extern float CalculateForceField([In] double[] positions, [Out] double[] forces);

        [DllImport("iSciPM6", EntryPoint = "Dispose")]
        private static extern void ExternDispose();

        [DllImport("iSciPM6", EntryPoint = "setQCMethod")]
        private static extern void SetQCMethod(int id);

        private IReporter reporter;
        private ISimulationBoundary simBoundary;
        private string paramsDir;
        private QCMethod method;

        private double[] forces;
        private double[] positions;
        private int numberOfParticles;

        public ScineForceField(ScineInstantiatedTerms terms, InstantiatedTopology topology, ISimulationBoundary simBoundary = null, IReporter reporter = null)
        {
            paramsDir = Helper.ResolvePath(terms.ParamsDirectory);
            if (Directory.Exists(paramsDir) == false)
            {
                throw new Exception($"Parameter directory for Scine forcefield does not exist: ${paramsDir}");
            }
            method = terms.Method;
            this.simBoundary = simBoundary;
            this.reporter = reporter;

            var elements = GenerateElements(topology);
            try
            {
                Init(paramsDir, topology.NumberOfParticles, elements);
                SetQCMethod((int)method);
            }
            catch(Exception e)
            {
                reporter.PrintException(e, "Exception thrown while trying to initialise Scine Force Field. ");
            }


            forces = new double[3 * topology.NumberOfParticles];
            positions = new double[3 * topology.NumberOfParticles];
            numberOfParticles = topology.NumberOfParticles;
        }

        private int[] GenerateElements(InstantiatedTopology topology)
        {
            int[] elements = new int[topology.NumberOfParticles];
            
            for(int i=0; i < topology.NumberOfParticles; i++)
            {
                elements[i] = PeriodicTable.GetElementProperties(topology.Elements[i]).Number;
            }
            return elements;
        }

        public string Name
        {
            get
            {
                return "Scine Force Field";
            }
        }

        public float CalculateForceField(IAtomicSystem system, List<Vector3> forcesOut)
        {
            //Convert particle positions from Vectors to flat arrays
            for (int i = 0; i < system.NumberOfParticles; i++)
            {
                forces[3 * i + 0] = 0f;
                forces[3 * i + 1] = 0f;
                forces[3 * i + 2] = 0f;

                Vector3 pos = system.Particles.Position[i];
                positions[3 * i + 0] = (double)pos.X;
                positions[3 * i + 1] = (double)pos.Y;
                positions[3 * i + 2] = (double)pos.Z;

            }
            //Run calculation
            double energy = 0;
            try
            {
                energy = CalculateForceField(positions, forces);
            }
            catch (Exception e)
            {
                reporter.PrintException(e, "Exception thrown trying to run Scine Force Field");
               
                throw;
            }
            //Copy forces back to simbox.
            CopyForces(forces, forcesOut, numberOfParticles);
            return (float)energy;
        }

        public void Dispose()
        {
            ExternDispose();
        }

        #region Private Methods

        /// <summary>
        /// Copies the forces from DFTB output to Narupa.
        /// </summary>
        /// <param name="input_forces">Forces.</param>
        /// <param name="output">Output.</param>
        /// <param name="numberOfAtoms">Number of atoms.</param>
        private void CopyForces(double[] input_forces, List<Vector3> output, int numberOfAtoms)
        {

            for (int i = 0; i < numberOfAtoms; i++)
            {
                Vector3 force = new Vector3((float)input_forces[3 * i + 0], (float)input_forces[3 * i + 1], (float)input_forces[3 * i + 2]);
                output[i] += force; 
            }
        }

        #endregion  
    }
}
